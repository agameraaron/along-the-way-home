extends Node

#manages what mode is currently in use
#manages music transitions

const version = [0,5,1]
const version_name = "Parent Control"
var permission_level = permission_roles.USER #debug, team, user

#debug values
var starting_player_character = "pillion"

enum permission_roles{DEBUG,TEAM,USER}

enum modes{START,TITLE,GAME,OPTIONS,MANUAL,CREDITS,ENDING}
var current_mode = modes.TITLE
var next_mode = modes.TITLE
var mode_change_step = 0

#music player management
var current_song_file_name = ""
var next_song_file_name = ""
var song_change_step = 0
var current_volume = 0
var music_transition_volume_change_rate = 0.008

#global save values
var sound_volume = 0.1
var music_volume = 0.5
var screen_size = "Max"




"""
Todo:
	
	- Have enemy react to seeing player, triggering being caught
	
	- Options mode
		- music & sound effects sliders
	
	- player is adult at start
	
	- If caught as adult, reset map to beginning & spawn player as adult
	
	- If caught as child, reset map & position to last checkpoint
	
	- Draw background
	- Draw parallax background
	
	
	
	- Profile save data (map & entrance)
		- When a checkpoint is ran across, it will be auto-used
		- It will save the profile data then disable itself
		- When a game is resumed, the player spawns at the last checkpoint used.
	
	
	
	- Socialable computer controlled officer in first map
		- Dialogue window
		- Social trigger
		- Read dialogue from translation friendly script
	
	
"""


func mode_change():
	if mode_change_step == 0: #set curtain to close in step 0
		if get_node("mode curtain/curtain/animator").get_current_animation() != "close curtain":
			get_node("mode curtain/curtain/animator").play("close curtain")
		mode_change_step = 1 #prepare for this animation to end in step 1
	elif mode_change_step == 1: #when animation ends set to step 2
		if get_node("mode curtain/curtain/animator").get_current_animation() != "close curtain":
			mode_change_step = 2
	elif mode_change_step == 2:
		var next_mode_title = ""
		#free current mode
		if has_node("mode"):
			get_node("mode").free()
		#instantiate next mode
		next_mode_title = modes.keys()[next_mode].to_lower()
		var next_mode_node = load("res://data/modes/"+next_mode_title+" mode.tscn").instance()
		#add next mode as child of program manager
		add_child(next_mode_node)
		#set mode's draw order by setting it's node heirarchy to the top
		move_child(next_mode_node,0)
		#draw curtain
		get_node("mode curtain/curtain/animator").play("draw curtain")
		current_mode = next_mode
		mode_change_step = 0
	

func _process(_delta):
	if current_mode != next_mode:
		mode_change()
	if current_song_file_name != next_song_file_name:
		transition_songs()

func _ready():
	if permission_level == permission_roles.TEAM or permission_level == permission_roles.USER:
		OS.set_window_maximized(true)
	elif permission_level == permission_roles.DEBUG:
		OS.set_window_size(Vector2(426,240)*2)
	get_node("music player").set_volume_db(linear2db(sound_volume))
	

func play_song(next_song_name):
	if next_song_name == "Silence":
		next_song_file_name = ""
	else:
		next_song_file_name = "res://assets/audio/music/"+next_song_name+".ogg"

func transition_songs():
	current_volume = get_node("music player").get_volume_db()
	current_volume = db2linear(current_volume)
	
	if song_change_step == 0: #turn down volume of currently playing song until practically mute
		current_volume -= music_transition_volume_change_rate
		if current_volume <= 0:
			song_change_step = 1
			current_volume = 0.0
		get_node("music player").set_volume_db(linear2db(current_volume))
	elif song_change_step == 1: #set new song
		if next_song_file_name == "":
			get_node("music player").stop()
		else:
			get_node("music player").set_stream(load(next_song_file_name))
			get_node("music player").play()
		song_change_step = 2
	elif song_change_step == 2: #turn volume up
		if current_volume < music_volume: #using saved music volume as reference for maximum value
			current_volume += music_transition_volume_change_rate
		else:
			current_volume = music_volume
			song_change_step = 3
		get_node("music player").set_volume_db(linear2db(current_volume))
	elif song_change_step == 3:
		current_song_file_name = next_song_file_name
		song_change_step = 0
	
