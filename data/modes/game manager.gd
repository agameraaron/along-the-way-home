extends Node

#debug
var debug_player = true

#manages map change outs
var current_map = "start"
onready var next_map = current_map
var next_map_entrance_number = 0
var change_map_step = 0

#keep track of the current map's node location
var current_map_node

#checkpoints
var teleport_to_checkpoint = false #triggered on load, death or when sighted
var checkpoint_map = ""

#in scope of this manager so it can be pulled in and out of safe space
onready var current_player_character_name = get_parent().starting_player_character
onready var next_player_character_name = current_player_character_name
onready var player_character = load("res://data/entities/beings/"+get_parent().starting_player_character+".tscn").instance()
onready var player_camera = get_node("cameras/player 1")
# Called when the node enters the scene tree for the first time.
func _ready():
	debug_setup()
	#current_map = ""
	var starting_map = load("res://data/maps/"+current_map+".tscn").instance()
	add_child(starting_map)
	current_map_node = starting_map
	checkpoint_map = starting_map
	
	starting_map.get_node("stage").add_child(player_character)
	player_character.set_position(starting_map.get_node("triggers/entrance "+str(next_map_entrance_number)).get_position())
	player_character.debug_entity = debug_player
	player_character.intelligence_source = player_character.intelligence_sources_available.USER
	#player_character.set_collision_layer_bit(0,1)
	#player_character.set_collision_layer_bit(1,0)
	
	#instance the player's camera
	#player_character.add_child(player_camera)
	#player_camera.set_name("player camera")
	player_camera.host = player_character
	player_camera._set_current(true)
	player_camera.set_offset(Vector2(0,-32))
	
	get_parent().play_song("Stage")
	

func _process(_delta):
	
	if next_map != current_map:
		change_map()
	check_for_show_debug_info_input()

func change_map():
	
	if change_map_step == 0: #drop curtain
		get_node("map curtain/curtain/animator").play("close curtain")
		change_map_step = 1
	elif change_map_step == 1: #wait for curtain to finish dropping
		if get_node("map curtain/curtain/animator").get_current_animation() != "close curtain":
			#print("done drawing map curtain")
			change_map_step = 2
	elif change_map_step == 2: #move player to safe space, free map, load new map
		player_character.get_parent().remove_child(player_character)
		if next_player_character_name != current_player_character_name: #change out with new character
			var new_player_character = load("res://data/entities/beings/"+next_player_character_name+".tscn").instance()
			player_camera.host = new_player_character
			player_character.queue_free()
			player_character = new_player_character
			current_player_character_name = next_player_character_name
		get_node("safe space").add_child(player_character)
		
		current_map_node.queue_free()
		current_map_node = load("res://data/maps/"+next_map+".tscn").instance()
		add_child(current_map_node)
		move_child(current_map_node,0)
		get_node("safe space").remove_child(player_character)
		current_map_node.get_node("stage").add_child(player_character)
		player_character.set_position(current_map_node.get_node("triggers/entrance "+str(next_map_entrance_number)).get_position())
		change_map_step = 3
	elif change_map_step == 3: #draw curtain
		get_node("map curtain/curtain/animator").play("draw curtain")
		current_map = next_map
		print("Loaded next map: "+str(next_map))
		change_map_step = 0
	

func check_for_show_debug_info_input():
	if get_parent().permission_level == get_parent().permission_roles.DEBUG or get_parent().permission_level == get_parent().permission_roles.TEAM:
		if Input.is_action_just_pressed("Show Debug Info"):
			if get_node("interfaces/debug").is_visible():
				get_node("interfaces/debug").hide()
			else:
				get_node("interfaces/debug").show()
			

func debug_setup():
	if get_parent().permission_level == get_parent().permission_roles.DEBUG:
		get_node("interfaces/debug").show()
