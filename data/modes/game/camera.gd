extends Camera2D

var host = null

func _physics_process(delta):
	if host:
		set_position(host.get_position())
