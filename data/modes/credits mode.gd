extends Node

var current_selection_number = 0

var menu_selection_offset = Vector2(-20,12)

func _ready():
	get_parent().play_song("Credits")
	get_node("interfaces/menu sounds").set_volume_db(linear2db(get_parent().sound_volume))

func menu_sound(sound_chosen):
	get_node("interfaces/menu sounds").set_stream(load("res://assets/audio/sounds/"+sound_chosen+".wav"))
	get_node("interfaces/menu sounds").play()

func _process(_delta):
	if Input.is_action_just_pressed("D-Pad Down"):
		current_selection_number += 1
		menu_sound("UIMove")
	elif Input.is_action_just_pressed("D-Pad Up"):
		current_selection_number -= 1
		menu_sound("UIMove")
	if current_selection_number > get_node("interfaces/user/options").get_child_count()-1:
		current_selection_number = 0
	if current_selection_number < 0:
		current_selection_number = get_node("interfaces/user/options").get_child_count()-1
	get_node("interfaces/user/selection cursor").set_position(get_node("interfaces/user/options").get_child(current_selection_number).get_global_position()+menu_selection_offset)
	
	
	#selections
	if Input.is_action_just_pressed("A"):
		if current_selection_number == 0: #back
			menu_sound("UISelect")
			get_parent().next_mode = get_parent().modes.OPTIONS
