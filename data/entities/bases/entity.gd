extends KinematicBody2D

#debug
export var debug_entity = false
#properties
#appearance
var facing_direction = 0 #left
#physics
export var max_walk_acceleration = Vector2(0,0)
export var max_run_acceleration = Vector2(0,0)
export var max_crawl_acceleration = Vector2(0,0)
var max_momentum = Vector2(100,100)
var forward_jump_force = 8000
#process
#physics
export var acceleration_rate = Vector2(0,0)
export var deceleration_speed = Vector2(0,0)
export var deceleration_margin = Vector2(0,0)
var momentum = Vector2(0,0)
var gravity_current = 0
const gravity_acceleration_rate = 210
const gravity_max = 22000
var is_airborne = false
#var is_jumping = false
var jumping_unstick_frames = 0
var is_crouching = false #includes crawling

#action states
enum action_states_available{MENU,NEUTRAL,CROUCHING,JUMPING,DEAD,DISABLED}
var action_state = action_states_available.NEUTRAL

func _ready():
	pass

func _process(_delta):
	get_node("/root/program manager/mode/interfaces/debug/is airborne").set_text("airborne: "+str(is_airborne))
	get_node("/root/program manager/mode/interfaces/debug/action state").set_text("player action state: "+str(action_states_available.keys()[action_state]))
	get_node("/root/program manager/mode/interfaces/debug/gravity").set_text(str(gravity_current))
func _physics_process(delta):
	
	if jumping_unstick_frames > 0:
		is_airborne = true
		jumping_unstick_frames -= 1
	else:
		is_airborne = not (test_move(transform,Vector2(0,2)))
	
	if is_airborne:
		if gravity_current < gravity_max:
			if action_state == action_states_available.JUMPING:
				gravity_current += gravity_acceleration_rate
			else:
				gravity_current += gravity_acceleration_rate*12
		elif gravity_current > gravity_max:
			gravity_current = gravity_max
	else:
		gravity_current = 0
		
	
	
	if is_airborne:
		move_and_slide(Vector2(momentum.x,momentum.y+gravity_current)*delta)
	else:
		move_and_slide_with_snap(Vector2(momentum.x,momentum.y+gravity_current)*delta,Vector2(0,1))
	
	if action_state == action_states_available.JUMPING:
		#just landed
		if is_airborne == false:# and not (test_move(transform,Vector2(0,2))):
			get_node("sprite").play("land")
			action_state = action_states_available.NEUTRAL
	
	#if gravity_current > -35:
		#gravity_current -= gravity_acceleration_rate
