extends "res://data/entities/bases/entity.gd"

export var is_sociable = false
export var starting_dialogue_index_name = ""

func _process(_delta):
	if is_sociable:
		for overlapping_body in get_node("talk area").get_overlapping_bodies():
			if "intelligence_source" in overlapping_body:
				if overlapping_body.intelligence_source == overlapping_body.intelligence_sources_available.USER:
					play_dialogue(starting_dialogue_index_name)

func play_dialogue(dialogue_index_name_requested):
	pass
