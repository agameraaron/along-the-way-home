extends "res://data/entities/bases/contact.gd"

export var being_name = ""

enum intelligence_sources_available{USER,COMPUTER,DUMMY}
#Determines source of control
export(intelligence_sources_available) var intelligence_source

#physics
var forward_jump_power = -7600
var high_jump_power = -8000


onready var sightray_exceptions = [get_node("push collider"),get_node("vitals area"),get_node("talk area")]


func _ready():
	
	for exception in sightray_exceptions:
		for raycount in range(1,4):
			get_node("sightrays/sightray "+str(raycount)).add_exception(exception)

func _process(_delta):
	
	user_input()
	
	debug_report()
	
	update_facing_direction()
	


func update_facing_direction():
	if facing_direction == 0:
		get_node("sprite").set_flip_h(true)
	else:
		get_node("sprite").set_flip_h(false)



func user_input():
	if intelligence_source == intelligence_sources_available.USER:
		
		if action_state == action_states_available.NEUTRAL:
			
			#Movement
			var is_running = false
			if Input.is_action_pressed("B"):
				is_running = true
			if Input.is_action_pressed("D-Pad Left"):
				facing_direction = 0
				if is_running:
					if get_node("sprite").get_animation() != "run":
						get_node("sprite").play("run")
					
					if momentum.x > -max_run_acceleration.x:
						momentum.x -= acceleration_rate.x
					else:
						momentum.x = -max_run_acceleration.x
				else:
					if get_node("animator").get_current_animation() != "walk":
						get_node("animator").play("walk")
					
					if momentum.x > -max_walk_acceleration.x:
						momentum.x -= acceleration_rate.x
					else:
						momentum.x = -max_walk_acceleration.x
				
				
			elif Input.is_action_pressed("D-Pad Right"):
				facing_direction = 1
				if is_running:
					if get_node("sprite").get_animation() != "run":
						get_node("sprite").play("run")
					
					if momentum.x < max_run_acceleration.x:
						momentum.x += acceleration_rate.x
					else:
						momentum.x = max_run_acceleration.x
				else:
					if get_node("animator").get_current_animation() != "walk":
						get_node("animator").play("walk")
					
					if momentum.x < max_walk_acceleration.x:
						momentum.x += acceleration_rate.x
					else:
						momentum.x = max_walk_acceleration.x
			else:
				get_node("animator").play("idle")
			
			#crouching
			if Input.is_action_just_pressed("D-Pad Down"):
				get_node("animator").play("crouch")
				action_state = action_states_available.CROUCHING
			#Jumping
			elif Input.is_action_just_pressed("A"):
				jumping_unstick_frames = 20
				gravity_current = forward_jump_power #jump power
				if Input.is_action_pressed("D-Pad Left"):
					facing_direction = 0
					momentum.x = -forward_jump_force
					if get_node("sprite").get_animation() != "jump forward":
						get_node("sprite").play("jump forward")
				elif Input.is_action_pressed("D-Pad Right"):
					facing_direction = 1
					momentum.x = forward_jump_force
					if get_node("sprite").get_animation() != "jump forward":
						get_node("sprite").play("jump forward")
				else:
					if get_node("sprite").get_animation() != "jump up":
						get_node("sprite").play("jump up")
						gravity_current = high_jump_power
				action_state = action_states_available.JUMPING
				#is_airborne = true #will set itself
		
		elif action_state == action_states_available.CROUCHING:
			if Input.is_action_pressed("D-Pad Left"):
				facing_direction = 0
				if get_node("animator").get_current_animation() != "crawl":
					get_node("animator").play("crawl")
				if momentum.x > -max_crawl_acceleration.x:
					momentum.x -= acceleration_rate.x
				else:
					momentum.x = -max_crawl_acceleration.x
			elif Input.is_action_pressed("D-Pad Right"):
				facing_direction = 1
				if get_node("animator").get_current_animation() != "crawl":
					get_node("animator").play("crawl")
				if momentum.x < max_crawl_acceleration.x:
					momentum.x += acceleration_rate.x
				else:
					momentum.x = max_crawl_acceleration.x
			if Input.is_action_just_released("D-Pad Down"):
				action_state = action_states_available.NEUTRAL
		
		#decelerate for a bit of slide
		if action_state != action_states_available.JUMPING:
			if momentum.x > deceleration_margin.x:
				momentum.x -= deceleration_speed.x
			elif momentum.x < -deceleration_margin.x:
				momentum.x += deceleration_speed.x
			else:
				momentum.x = 0

func debug_report():
	
	if get_node("/root/program manager").permission_level == get_node("/root/program manager").permission_roles.DEBUG and debug_entity == true:
		get_node("/root/program manager/mode/interfaces/debug/acceleration").set_text(str(acceleration_rate))
		get_node("/root/program manager/mode/interfaces/debug/momentum").set_text(str(momentum))
