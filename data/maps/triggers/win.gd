extends Area2D

export var allowed_to_win = "pillion"

func _ready():
	pass

func _process(_delta):
	for body_scanning in get_overlapping_bodies():
		if "intelligence_source" in body_scanning:
			if body_scanning.intelligence_source == body_scanning.intelligence_sources_available.USER:
				if allowed_to_win == body_scanning.being_name:
					get_node("/root/program manager").next_mode = get_node("/root/program manager").modes.ENDING
