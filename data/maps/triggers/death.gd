extends Area2D


func _ready():
	pass

func _process(_delta):
	for body_scanning in get_overlapping_bodies():
		if "intelligence_source" in body_scanning:
			if body_scanning.intelligence_source == body_scanning.intelligence_sources_available.USER:
				if body_scanning.being_name == "mother":
					get_node("/root/program manager/mode").current_map = ""
					#get_node("/root/program manager/mode").next_map = get_node("/root/program manager/mode").checkpoint_map
					get_node("/root/program manager/mode").teleport_to_checkpoint = true
				else:
					get_node("/root/program manager/mode").next_player_character_name = "mother"
					get_node("/root/program manager/mode").next_map = "inside house"
