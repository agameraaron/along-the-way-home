extends Area2D

export var next_map_name = ""
export var next_entrance_number = 0
export var depthward_entryway = false


func _ready():
	pass

func _process(_delta):
	for body_scanning in get_overlapping_bodies():
		if "intelligence_source" in body_scanning:
			if body_scanning.intelligence_source == body_scanning.intelligence_sources_available.USER:
				if depthward_entryway:
					if Input.is_action_just_pressed("D-Pad Up"):
						get_node("/root/program manager/mode").next_map = next_map_name
						get_node("/root/program manager/mode").next_map_entrance_number = next_entrance_number
						#update checkpoint to be beginning of map
						get_node("/root/program manager/mode").checkpoint_map = next_map_name
				else:
					get_node("/root/program manager/mode").next_map = next_map_name
					get_node("/root/program manager/mode").next_map_entrance_number = next_entrance_number
					#update checkpoint to be beginning of map
					get_node("/root/program manager/mode").checkpoint_map = next_map_name
				
